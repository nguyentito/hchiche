{-# LANGUAGE OverloadedStrings, ViewPatterns, PatternGuards #-}

module Processing (normalize, bindingsToParser, bindingsToPrinter) where

import Control.Applicative
import Control.Monad
import Data.List
import qualified Data.Map as Map
import Data.Monoid
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.Builder as TB
import qualified Data.Text.Lazy.Builder.Int as TB
import Data.Tuple
import Text.Parsec hiding ((<|>))
import Text.Parsec.Text.Lazy

import AST
import ASTNorm
import Datum

normalize :: (CustomAssoc, [BindSep]) -> (NCustomAssoc, NBindings)
normalize (c, bs) =
  let c' = normalizeCustoms c in (c', normalizeBindings c' bs)


-- there's a little trick here, should write it up (TODO)
normalizeCustoms :: CustomAssoc -> NCustomAssoc
normalizeCustoms = map head . tail . scanl f []
  where f normalizedPrefix (name, customFormat) =
          (name, nCustomFormat):normalizedPrefix
          -- note: normalizedPrefix is actually the reverse of the prefix
          where nCustomFormat = case customFormat of
                  Enum cases -> Enum cases
                  Record bindings ->
                    Record (normalizeBindings normalizedPrefix bindings)
                  Tuple  bindings ->
                    Tuple  (normalizeBindings normalizedPrefix bindings)
          
  

-- TODO TODO TODO
-- Bon, pour l'instant c'est moche, je fais des recalculs redondants
-- (-> c'est toujours le cas ? CHECK)
-- plutôt que d'employer une monade Reader comme il faudrait
-- Il faudrait aussi normaliser les types custom...
-- Autres saletés dans le code : les fonctions partielles partout

-- CHECK: replace foldr by foldl'?
normalizeBindings :: NCustomAssoc -> [BindSep] -> NBindings
normalizeBindings custom = foldr f nbEmpty
  where f (Sep  sep         ) bindings = sep `nbConsSep` bindings
        f (Bind (ident, fmt)) bindings =
          -- is there a lens on the 2nd component of a triplet?
          -- should we add a dependency on Lens?
          let (leading, nFmt, trailing) = normalizeFormat custom fmt in
          (leading, (ident, nFmt), trailing) `nbCons` bindings

normalizeFormat :: NCustomAssoc -> Format -> (Separator, NFormat, Separator)
normalizeFormat custom fmt = (preSep fmt, nFmt, postSep fmt)
  where
    preSep (FPrim p) = case p of
      PUInt   -> Space
      PSInt   -> Space
      PChar   -> NullSep
      PDigit  -> NullSep
      PLine _ -> Newline
      PWord _ -> Space
    preSep FArray  { } = Newline
    preSep FMatrix { } = Newline

    preSep (FCustom name) = case lookup name custom of
      Just (Enum _)          -> NullSep
      Just (Record bindings) -> leadingSep bindings
      Just (Tuple  bindings) -> leadingSep bindings
      Nothing -> error "TODO: handle possiblity of error"

    postSep (FCustom name) = case lookup name custom of
      Just (Enum _)         -> NullSep
      Just (Record bindings) -> trailingSep bindings
      Just (Tuple  bindings) -> trailingSep bindings
      Nothing -> error "TODO: handle possiblity of error"
    -- the following is true for all non-custom formats:
    postSep fmt = preSep fmt

    nFmt = case fmt of
      FPrim p -> NPrim p
      FCustom name -> NCustom name
      FArray lenExpr   maybeVar  bindings ->
        let nbs@(nbView -> NBSingle nb) = normalizeBindings custom bindings
        in NArray  lenExpr  maybeVar  nb (trailingSep nbs <> leadingSep nbs)
      FMatrix dimExprs maybeVars bindings ->
        let nbs@(nbView -> NBSingle nb) = normalizeBindings custom bindings
        in NMatrix dimExprs maybeVars nb (trailingSep nbs <> leadingSep nbs)


-- Reference parser

bindingsToParser :: NCustomAssoc -> NBindings -> Parser Fields
bindingsToParser custom = f Map.empty
  where
    f :: Fields -> NBindings -> Parser Fields
    f acc (nbView -> NBEmpty) = pure acc
    -- leave to the surroundings the consumption of the trailing space
    f acc (nbView -> NBSingle (ident, fmt)) =
      flip (Map.insert ident) acc <$> g acc fmt
    f acc (nbView -> NBCons (ident, fmt) sep rest) = do
      val <- g acc fmt
      consumeSep sep
      f (Map.insert ident val acc) rest

    consumeSep NullSep = pure ()
    consumeSep Space   = (void $ char ' ')
    -- should we be more strict and forbid "\r\n"?
    consumeSep Newline = do
      (void . try $ string "\r\n") <|> (void $ char '\n')
      -- /!\ == no space after a newline allowed! == /!\
      -- actually, two consecutive white space characters should
      -- be forbidden in all situations
      notFollowedBy $ oneOf " \r\n"

    -- strange combination of `sequence` and `intersperse`
    intersperseSep _   []     = pure []
    intersperseSep _   [x]    = (:[]) <$> x
    intersperseSep sep (x:xs) = (:) <$> x <*>
                                (consumeSep sep *> intersperseSep sep xs)

    readUInt :: Parser Int
    readUInt = read <$> many1 digit
    
    readSInt :: Parser Int
    readSInt = do sign <- (char '-' >> return negate)
                          <|> (char '+' >> return id)
                          <|> return id
                  modulus <- readUInt
                  return $ sign modulus

    readDigit :: Parser Int
    readDigit = read . (:[]) <$> digit
    

    g :: Fields -> NFormat -> Parser Datum
    -- could be called "formatToParser"
    g _   (NPrim PUInt ) = DInt <$> readUInt
    g _   (NPrim PSInt ) = DInt <$> readSInt
    g _   (NPrim PChar ) = DChar <$> anyChar
    g _   (NPrim PDigit) = DInt <$> readDigit
    
    g ctx (NPrim (PLine lenExpr)) = do
      str <- manyTill anyChar $ lookAheadSep [Newline]
      -- CHECK: do we want lenExpr to be the *exact* length,
      --        or just an upper bound on the length?
      guard $ length str <= evalExpr ctx lenExpr
      pure $ DString str
    g ctx (NPrim (PWord lenExpr)) = do
      str <- manyTill anyChar $ lookAheadSep [Newline, Space]
      guard $ length str <= evalExpr ctx lenExpr
      pure $ DString str
      
    g ctx arr@(NArray {}) = do
      let len = evalExpr ctx (nArrLen arr)
          (_, fmt) = nArrElt arr
          parserList = case nArrVar arr of
            Nothing -> replicate len . g ctx $ fmt
            Just var -> flip map [0..(len-1)] $ \i ->
              g (Map.insert var (DInt i) ctx) fmt
      list <- intersperseSep (nArrSep arr) parserList
      lookAheadSep [Newline]
      pure $ DArray list
      
    -- for this prototype, we represent a matrix as an array of arrays
    g ctx mat@(NMatrix {}) = do
      (DArray list) <-
        g ctx NArray { nArrLen = fst . nMatDim $ mat
                     , nArrVar = fst <$> nMatVars mat
                     , nArrSep = Newline
                     , nArrElt = ("row", NArray { nArrLen = snd . nMatDim $ mat
                                                , nArrVar = snd <$> nMatVars mat
                                                , nArrSep = nMatSep mat
                                                , nArrElt = nMatElt mat
                                                })
                     }
      pure . DMatrix . map (\(DArray l) -> l) $ list

    -- forget context when parsing custom type
    g _ (NCustom name) = case lookup name custom of
      Just (Enum cases) ->
        choice
        . map (\(c, enumCase) -> DEnum name enumCase <$ char c)
        $ cases
      Just (Record nbindings) ->
        (DRecord name <$>)
        . bindingsToParser custom
        $ nbindings
      Just (Tuple nbindings) -> -- same as Record
        (DRecord name <$>)
        . bindingsToParser custom
        $ nbindings
      

    lookAheadSep = lookAhead . (eof <|>) . choice . map consumeSep

evalExpr :: Fields -> IntExpr -> Int
evalExpr ctx = f
  where f (IConst x)   = x
        f (IVar ident) = let (DInt x) = ctx Map.! ident in x
        f (IOp op e1 e2) = evalOp op (f e1) (f e2)
        evalOp Add = (+)
        evalOp Sub = (-)
        evalOp Mul = (*)


-- Reference printer

bindingsToPrinter :: NCustomAssoc -> NBindings -> Fields -> T.Text
bindingsToPrinter custom bindings fields = TB.toLazyText $ f fields bindings
  where f _   (nbView -> NBEmpty) = mempty
        f ctx (nbView -> NBSingle (ident, fmt)) =
          printDatum fmt (ctx Map.! ident)
        f ctx (nbView -> NBCons (ident, fmt) sep rest) =
          printDatum fmt (ctx Map.! ident)
          <> printSep sep
          <> f ctx rest
          
        printDatum (NPrim PUInt)  (DInt  n) = TB.decimal n
        printDatum (NPrim PSInt)  (DInt  n) = TB.decimal n
        printDatum (NPrim PChar)  (DChar c) = TB.singleton c
        printDatum (NPrim PDigit) (DInt  n) = TB.decimal n
        -- we don't care about the length of the strings
        printDatum (NPrim (PLine _)) (DString str) = TB.fromString str
        printDatum (NPrim (PWord _)) (DString str) = TB.fromString str
                                                     
        printDatum (NArray { nArrElt = (_,fmt), nArrSep = sep }) (DArray list) =
          mconcat . intersperse (printSep sep) . map (printDatum fmt) $ list
        printDatum (NMatrix { nMatElt = (_,fmt), nMatSep = sep }) (DMatrix listlist) =
          mconcat . intersperse (printSep Newline)
          . map (mconcat . intersperse (printSep sep) . map (printDatum fmt))
          $ listlist

        printDatum (NCustom name) (DEnum name' enumCase)
          -- should also check name == name'
          | Just (Enum assoc) <- lookup name custom,
            Just c <- lookup enumCase . map swap $ assoc =
              TB.singleton c

        printDatum (NCustom name) (DRecord name' recFields)
          -- should also check name == name'
          | Just (Record recBindings) <- lookup name custom =
            f recFields recBindings
          | Just (Tuple tupleBindings) <- lookup name custom =
            f recFields tupleBindings

        printDatum _ _ = error "Type error in the datum to be printed."
        
        printSep NullSep = mempty
        printSep Space   = TB.singleton ' '
        printSep Newline = TB.singleton '\n'

