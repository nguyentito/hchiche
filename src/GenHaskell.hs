{-# LANGUAGE OverloadedStrings, ViewPatterns #-}

module GenHaskell where

import Control.Arrow
import Data.Monoid
import qualified Data.Text.Lazy as T hiding (singleton)
import qualified Data.Text.Lazy.Builder as T
import qualified Data.Text.Lazy.Builder.Int as T
import qualified Data.Text as TS
import qualified Text.PrettyPrint.Leijen.Text as PP
import PPOperators

import AST
import ASTNorm
import Processing

{-
Common situations:
* 1 integer on a line
* 2 integers on a line
* 1 string on a line
* array of ints
* matrix of chars/enums
-}

-- We'll assume that the internal separators of a composite
-- are weaker than the leading/trailing separator
-- TODO: go and change the rules to enforce this in Processing
-- We also assume that leading sep = trailing sep

genHaskell :: HChiche -> PP.Doc
genHaskell hchiche = imports
                     ^$$^ fnDecl
                     ^$$^ ppt "main :: IO ()"
                     ^$^  (ppt "main = do" ^/-^ mainBody)
                     
  where imports = ppt "import Data.List"
        fnDecl = mainFnName <>
                 PP.align (PP.encloseSep (ppt " :: ") mempty (ppt " -> ")
                           . map ppt $ argTypes ++ ["IO ()"])
                 ^$^ mainFnCall
                 ^+^ ppc '=' ^+^ ppt "undefined"
        
        (argNames, argTypes) =
          unzip . map (T.pack *** fmtType) . nBindingsList $ nArgs
        
        mainBody = readArgs ^$^ mainFnCall

        mainFnCall = mainFnName ^+^ ppt (T.unwords argNames)
        
        readArgs = bindingsToReader nCustomAssoc nArgs

        (nCustomAssoc, nArgs) =
          normalize (customTypes hchiche, mainFunctionArgs hchiche)
        mainFnName = pps $ mainFunctionName hchiche


fmtType :: NFormat -> T.Text
fmtType (NPrim p) = case p of
  PUInt   -> "Integer" -- use Z instead of Z/2^29 by default...
  PSInt   -> "Integer"
  PChar   -> "Char"
  PDigit  -> "Integer"
  PLine _ -> "String"
  PWord _ -> "String"
fmtType (NCustom typename) = undefined $ typename
fmtType (NArray  { nArrElt = (_, fmt) }) = "["<>fmtType fmt<>"]"
-- list of lists: super inefficient, TODO replace with stdlib arrays
fmtType (NMatrix { nMatElt = (_, fmt) }) = "[["<>fmtType fmt<>"]]"

bindingsToReader :: NCustomAssoc -> NBindings -> PP.Doc
bindingsToReader ctx = PP.hsep . map lineReader . bindingsLines ctx

data Line = SingleLine NBindings
          | MultiLine  NBinding

bindingsLines :: NCustomAssoc -> NBindings -> [Line]
bindingsLines _   (nbView -> NBEmpty) = []
bindingsLines ctx (nbView -> NBSingle b@(_,fmt))
  | isMultiLine ctx fmt = [MultiLine b]
  | otherwise           = [SingleLine $ nbSingleBind b]
bindingsLines ctx (nbView -> NBCons b sep b')
  | sep == Newline = bindingsLines ctx (nbSingleBind b)
                     ++ bindingsLines ctx b'
  | otherwise = let (SingleLine bs : rest) = bindingsLines ctx b' in
                SingleLine (b `nbConsBind` (sep `nbConsSep` bs)) : rest
  
isMultiLine :: NCustomAssoc -> NFormat -> Bool
isMultiLine _   (NPrim _) = False
isMultiLine ctx (NCustom typename) =
  let (Just custom) = lookup typename ctx in
  case custom of
    Enum _ -> False
    Record bs -> case bindingsLines ctx bs of -- TODO: don't recompute
                   [SingleLine _] -> True
                   _              -> False
    Tuple  bs -> case bindingsLines ctx bs of
                   [SingleLine _] -> True
                   _              -> False
  

-- Lots and lots of special cases
lineReader :: Line -> PP.Doc
lineReader (SingleLine _) = undefined

                         
