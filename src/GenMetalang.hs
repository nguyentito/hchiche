{-# LANGUAGE OverloadedStrings, Rank2Types, ViewPatterns #-}

module GenMetalang (genMetalang) where

import Control.Applicative
import Data.Monoid
import qualified Data.Text.Lazy as T hiding (singleton)
import qualified Data.Text.Lazy.Builder as T
import qualified Data.Text.Lazy.Builder.Int as T
import qualified Data.Text as TS
import qualified Text.PrettyPrint.Leijen.Text as PP
import qualified Formatting as F
import qualified Formatting.Holey as FI
import Formatting ((%))
import PPOperators

import AST
import ASTNorm
import Processing


-- the myriad of text-related types in this code
-- is actually pretty annoying...
-- can we hope for a "printer combinator" library
-- which would susbsume all this? <insert xkcd 'standards' here>


genMetalang :: HChiche -> PP.Doc
genMetalang hchiche = prelude
                      ^$^ mainFnDecl
                      ^$^ ppt "main"
                      <> blockEnd (readArgs ^$^ mainFnCall)
                      <> doubleLine
  where
    prelude = dataTypeDecls nCustomAssoc
    mainFnDecl = mainFnProto
                 <> blockEnd (ppt "/* insérez votre code ici */")
                 <> PP.line
    mainFnCall = mainFnName <> callArgs

    readArgs = bindingsToReader nArgs
    callArgs = argsList . map (pps . fst) -- extract name from (name, fmt)
               . nBindingsList $ nArgs
    mainFnProto = ppt "def " <> mainFnName
                  <> (argsList . map bindingToDef . nBindingsList $ nArgs)

    (nCustomAssoc, nArgs) =
      normalize (customTypes hchiche, mainFunctionArgs hchiche)
    mainFnName = pps $ mainFunctionName hchiche

-- for this function, encloseSep doesn't really do what we want...
argsList :: [PP.Doc] -> PP.Doc
argsList args = PP.lparen <> PP.align (f args) <> PP.rparen
  where f []     = mempty
        f [x]    = x
        f (x:xs) = x <> PP.comma <> PP.softline <> f xs

-- there's an unfortunate naming collision between
-- the "Formatting.format" function (name taken from Common Lisp?)
-- and the use of the noun "format" to refer to the part of a binding
-- on the left of a colon
-- we try to limit the use of F.format to here
ppf :: FI.Holey T.Builder PP.Doc a -> a
ppf m = FI.runHM m (ppt . T.toLazyText)

doubleLine = PP.line <> PP.line

blockEnd body =
  PP.nest 2 (PP.line <> body) ^$^ ppt "end"

typedecl keyword name body =
  ppt keyword ^+^ pps ('@':name) <> blockEnd body

dataTypeDecls :: NCustomAssoc -> PP.Doc
dataTypeDecls = PP.encloseSep mempty doubleLine doubleLine . map f
  where f (name, thing) = decl name thing
                          <> doubleLine
                          <> reader name thing

        -- this code is actually unreadable!
        decl name (Enum cases) =
          typedecl "enum" name
          . PP.hsep
          . map (pps . snd) $ cases

        decl name (Record fields) =
          typedecl "record" name
          . PP.vsep
          . map bindingToDef
          . nBindingsList $ fields

        decl name (Tuple fields) = decl name (Record fields)

        reader name thing = ppf ("def @"%%" read_"%%"()") name name
                            <> blockEnd (customReader name thing)


bindingToDef (name, fmt) = ppt (fmtType fmt) ^+^ pps name

tfrom :: (a -> T.Text) -> F.Format a
tfrom f = F.later (T.fromLazyText . f)

sfrom :: (a -> String) -> F.Format a
sfrom f = F.later (T.fromText . TS.pack . f)


x %% y = x % F.string % y

bindingsToReader :: NBindings -> PP.Doc
bindingsToReader (nbView -> NBEmpty) = mempty
bindingsToReader (nbView -> NBSingle (ident, fmt)) =
  -- leave to the surroundings the consumption of the trailing space
  formatToReader ident fmt
bindingsToReader (nbView -> NBCons (ident, fmt) sep rest) =
  formatToReader ident fmt
  <> consumeSep sep
  ^$^ bindingsToReader rest

consumeSep :: Separator -> PP.Doc
consumeSep NullSep = mempty
consumeSep _       = PP.softline <> ppt "skip"
      
formatToReader :: String -> NFormat -> PP.Doc

formatToReader var (NPrim PUInt)  = ppt "def read int"  ^+^ pps var
formatToReader var (NPrim PSInt)  = ppt "def read int"  ^+^ pps var
formatToReader var (NPrim PChar)  = ppt "def read char" ^+^ pps var
formatToReader var (NPrim PDigit) = ppt "def read int"  ^+^ pps var
    
formatToReader var (NPrim (PLine lenExpr)) =
  ppf ("def array<char> "%%"["%tfrom intExpr%"] = read_char_line()") var lenExpr
formatToReader var (NPrim (PWord lenExpr)) = do
  -- HACK, WRONG
  -- doesn't work if there are multiple words on a single line
  formatToReader var (NPrim (PLine lenExpr))
      
formatToReader var arr@(NArray {}) =
  let (elt, fmt) = nArrElt arr in
  case nArrVar arr of
    Nothing -> error "index omission not supported yet"
    Just ix ->
      ppf ("def array<"%tfrom fmtType%"> "%%"["%tfrom intExpr%"] with "%%" do")
          fmt var (nArrLen arr) ix
      <> blockEnd (formatToReader elt fmt
                   <> consumeSep (nArrSep arr)
                   ^$^ pps ("return "++elt))
      
    -- matrix = array of arrays (no matrix type in metalang)
formatToReader var mat@(NMatrix {}) =
  formatToReader var
  $ NArray { nArrLen = fst . nMatDim $ mat
           , nArrVar = fst <$> nMatVars mat
           , nArrSep = Newline
           , nArrElt = (var ++ "_row",
                        NArray { nArrLen = snd . nMatDim $ mat
                               , nArrVar = snd <$> nMatVars mat
                               , nArrSep = nMatSep mat
                               , nArrElt = nMatElt mat
                               })
           }

formatToReader var (NCustom name) =
  ppf ("def @"%%" "%%" = read_"%%"()") name var name
            

fmtType :: NFormat -> T.Text
fmtType (NPrim p) = case p of
  PUInt   -> "int"
  PSInt   -> "int"
  PChar   -> "char"
  PDigit  -> "int"
  PLine _ -> "array<char>" -- until metalang gets a read_string macro
  PWord _ -> "array<char>"
fmtType (NCustom typename) = T.pack $ '@':typename
fmtType (NArray  { nArrElt = (_, fmt) }) = "array<"<>fmtType fmt<>">"
fmtType (NMatrix { nMatElt = (_, fmt) }) = "array<array<"<>fmtType fmt<>">>"

intExpr :: IntExpr -> T.Text
intExpr = T.toLazyText . f
  where f (IConst n) = T.decimal n
        f (IVar   x) = T.fromString x
        f (IOp binop left right) =
          T.singleton '(' <>
          f left <> T.fromString (binopName binop) <> f right
          <> T.singleton ')'
        

customReader :: String -> NCustomFormat -> PP.Doc

customReader name (Enum cases) =
  formatToReader "c" (NPrim PChar)
  -- hope the enum is not empty...
  ^$^ (PP.vsep . map f $ cases)
  where f (char, constant) =
          ppf ("if c == '"%%"' then") [char]
          <> blockEnd (pps $ "return "++constant)
        
customReader name (Record bindings) =
  bindingsToReader bindings
  ^$^ ppf ("def @"%%" "%%"_ = record") name name
  <> blockEnd (PP.vsep
               . map (\(x, _) -> pps $ x ++ "=" ++ x)
               . nBindingsList
               $ bindings)
  ^$^ ppt "return" ^+^ pps (name++"_")

customReader name (Tuple bs) = customReader name (Record bs)
              
