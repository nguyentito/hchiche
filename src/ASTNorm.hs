module ASTNorm where

import Data.Monoid

import AST

-- N for normalized

type NBinding = (String, NFormat)

-- A list of bindings with
-- * a leading separator
-- * a trailing separator
-- * a list bind, sep, bind, sep, ..., bind OR nil
-- 
-- you should use smart constructors & view patterns to manipulate this type

data NBindings = NBindings NBList Separator
               deriving (Eq, Ord, Show)

data NBList = NBConsLeadingSep Separator NBinding NBList
            | NBNil
            deriving (Eq, Ord, Show)

leadingSep, trailingSep :: NBindings -> Separator
trailingSep (NBindings _ sep) = sep
leadingSep  (NBindings (NBConsLeadingSep sep _ _) _) = sep
leadingSep  (NBindings NBNil sep) = sep
-- note that an empty list of binders which forces a separator to appear
-- actually makes sense, so 'NBindings NBNil Space' is not meaningless

nbCons :: (Separator, NBinding, Separator) -> NBindings -> NBindings
nbCons (l, b, t) nbindings =
  l `nbConsSep` (b `nbConsBind` (t `nbConsSep` nbindings))

nbConsBind :: NBinding -> NBindings -> NBindings
nbConsBind binding (NBindings list trailing) =
  NBindings (NBConsLeadingSep NullSep binding list) trailing
  
nbConsSep :: Separator -> NBindings -> NBindings
nbConsSep sep (NBindings list trailing) = case list of
  NBNil -> NBindings NBNil (sep <> trailing)
  NBConsLeadingSep leading binding rest ->
    NBindings (NBConsLeadingSep (sep <> leading) binding rest) trailing
    
nbEmpty :: NBindings
nbEmpty = NBindings NBNil NullSep

nbSingleBind :: NBinding -> NBindings
nbSingleBind = flip nbConsBind nbEmpty

data NBView = NBCons NBinding Separator NBindings
            | NBSingle NBinding
            | NBEmpty
            deriving (Eq, Ord, Show)

nbView :: NBindings -> NBView
nbView (NBindings nbl endSep) = case nbl of
  NBConsLeadingSep _ b bs@(NBConsLeadingSep midSep _ _) ->
    NBCons b midSep (NBindings bs endSep)
  NBConsLeadingSep _ b NBNil -> NBSingle b
  NBNil -> NBEmpty

nBindingsList :: NBindings -> [NBinding]
nBindingsList (NBindings list _) = f list
  where f NBNil                        = []
        f (NBConsLeadingSep _ b rest) = b : f rest


type NCustomFormat = CustomFormat' NBindings
type NCustomAssoc = [(String, NCustomFormat)]

data NFormat = NPrim PrimFormat
             | NCustom String
             | NArray { nArrLen :: IntExpr
                      , nArrVar :: Maybe String
                      , nArrElt :: NBinding
                      , nArrSep :: Separator }
             | NMatrix { nMatDim  :: (IntExpr, IntExpr)
                       , nMatVars :: Maybe (String, String)
                       , nMatElt  :: NBinding
                       , nMatSep  :: Separator }
             deriving (Eq, Ord, Show)

