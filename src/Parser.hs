module Parser (hchicheParser) where

import Prelude hiding (lex)

import Control.Monad
import Control.Applicative

import Text.Parsec hiding (token, (<|>), many)
import Text.Parsec.String
import Text.Parsec.Language
import Text.Parsec.Token
import Text.Parsec.Expr

import AST

hchicheParser :: Parser HChiche
hchicheParser = whiteSpace lex *> hchiche <* eof

-- Lexing

lex :: TokenParser st
lex = makeTokenParser langDef
  where langDef = emptyDef { commentStart = "/*"
                           , commentEnd   = "*/"
                           , commentLine  = "//"
                           , nestedComments = False
                           , identStart = letter <|> char '_'
                           , identLetter = identStart langDef <|> digit
                           -- no user-defined operators
                           , opStart = parserZero
                           , opLetter = parserZero
                           , reservedOpNames = operatorList
                           , reservedNames = keywordList
                           , caseSensitive = True
                           }

keywordList, operatorList :: [String]
keywordList = ["record", "tuple", "enum", "input", "end",
               "uint", "sint", "char", "digit",
               "line", "word",
               "array", "matrix"]
operatorList = map binopName binops ++ ["=>"]

-- Parser

hchiche :: Parser HChiche
hchiche = do
  types <- many customType
  (funName, funArgs) <- input
  pure $ HChiche types funName funArgs

customType :: Parser (String, CustomFormat)
customType = choice . map typeDecl
             $ [ ("record", Record <$> bindingList)
               , ("tuple",  Tuple  <$> bindingList)
               , ("enum",   Enum   <$> enumParser)
               ]
  where typeDecl (keyword, bodyParser) =
          topLevelBlock keyword . liftA2 (,) typename $ bodyParser
        enumParser = many1 $ do
          c <- charLiteral lex
          reservedOp lex "=>"
          name <- identifier lex
          pure (c, name)
        typename = char '@' *> identifier lex

topLevelBlock :: String -> Parser a -> Parser a
topLevelBlock keyword parser =
  reserved lex keyword *> parser <* reserved lex "end"

input :: Parser (String, [BindSep])
input = topLevelBlock "input" $
        (,) <$> identifier lex <*> bindingList

bindingList :: Parser [BindSep]
bindingList = many (Sep <$> sep <|> Bind <$> binding)
  where sep = Space <$ comma lex <|> Newline <$ semi lex
        binding = do ident <- identifier lex
                     colon lex
                     fmt <- format
                     pure (ident, fmt)

format :: Parser Format
format = custom <|> primitive <|> strings <|> array <|> matrix
  where custom = char '@' *> (FCustom <$> identifier lex)
        primitive = choice . map (\(kw, prim) -> FPrim prim <$ reserved lex kw) $
            [ ("uint" , PUInt)
            , ("sint" , PSInt)
            , ("char" , PChar) 
            , ("digit", PDigit) 
            ]
        oneIntArg = parens lex intexpr
        strings = choice . map (\(kw, f) -> reserved lex kw
                                            *> (FPrim . f <$> oneIntArg)) $
            [ ("line" , PLine)
            , ("word" , PWord) 
            ]
        optionalArg = optionMaybe . brackets lex
        bindingsArg = braces lex bindingList
        array = do reserved lex "array"
                   len <- oneIntArg
                   var <- optionalArg $ identifier lex
                   elt <- bindingsArg
                   pure $ FArray len var elt
        matrix = do reserved lex "matrix"
                    nm <- parens lex . commaTuple $ intexpr
                    ij <- optionalArg . commaTuple $ identifier lex
                    elt <- bindingsArg
                    pure $ FMatrix nm ij elt
        commaTuple p = (,) <$> p <*> (comma lex *> p)
                   
intexpr :: Parser IntExpr
intexpr = buildExpressionParser table term
  where term = IConst . fromInteger <$> decimal lex
               <|> IVar <$> identifier lex
        table = [ [binop Mul]
                , [binop Add, binop Sub]
                ]
        binop op = Infix (IOp op <$ reservedOp lex (binopName op)) AssocLeft

