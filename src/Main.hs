import Control.Applicative
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.IO as TIO
import qualified System.Environment
import System.IO
import Text.Parsec
import qualified Text.PrettyPrint.Leijen.Text as PP

import AST
import Datum
import Parser
import Processing
import GenMetalang

main :: IO ()
main = do
  argv <- System.Environment.getArgs
  specFile <- readFile (head argv)
  case parse hchicheParser (head argv) specFile of
    Left err -> hPrint stderr err
    Right hchiche -> do
      let normalized = normalize (custom, mainFunctionArgs hchiche)
          custom = customTypes hchiche
      hPrint stderr normalized
      -- inputStream <- TIO.hGetContents stdin
      -- case parse (bindingsToParser custom normalized) "input" inputStream of
      --   Left err -> hPrint stderr err
      --   Right fields -> TIO.putStrLn . bindingsToPrinter custom normalized $ fields
      PP.displayIO stdout . PP.renderPretty 0.8 80 $ genMetalang hchiche

      
