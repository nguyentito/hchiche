{-# LANGUAGE OverloadedStrings #-}

module GenOCaml where

import qualified Data.Map as Map
import Data.Maybe
import Data.Monoid
import qualified Data.Text.Lazy as T
import qualified Text.PrettyPrint.Leijen.Text as PP
import PPOperators

import AST
import ASTNorm
import Processing


letBinding ident args body =
  PP.text "let" ^+^ str ident ^+^ PP.align (funargs args) <> PP.text "="
  ^/-^ body

letIn ident args body = PP.group $ letBinding ident args body ^$^ PP.text "in"

letRecIn ident = letIn ("rec " ++ ident) -- LOL

mainFun body = PP.text "let () =" <> PP.nest 2 (PP.line <> body)


readInt  = PP.text "read_int ()"
readLine = PP.text "read_line ()"

-- funargs adds a trailing space if args /= []
funargs [] = mempty
funargs x  = PP.encloseSep mempty PP.space PP.softline . map str $ x
funcall fn args = str fn ^+^ funargs args

example = mainFun $
          letIn "n" [] readInt
          ^$^ letIn "m" [] readInt
          ^$^ funcall "addition" ["n", "m"]

doubleLine = PP.line <> PP.line

stupidReaderSynth :: NBindings -> PP.Doc
stupidReaderSynth = undefined

dataTypeDecl :: CustomMap -> PP.Doc
dataTypeDecl = PP.encloseSep mempty doubleLine doubleLine
               . map (uncurry f) . Map.toAscList
  where f name (Enum cases) =
          PP.text "type" ^+^ str name ^+^ PP.char '='
          ^+^ (PP.encloseSep mempty mempty (PP.text " | ")
               . map (str. snd) $ cases)

        f name (Record fields) =
          PP.text "type" ^+^ str name ^+^ PP.char '='
          ^+^ (PP.group . PP.align $ recordFields fields)
        recordFields = PP.encloseSep
                       (str "{ ") (str " }")
                       (PP.char ';' <> PP.softline)
                       . map fieldDecl . mapMaybe getBinding
        fieldDecl (name, fmt) = str name ^+^ PP.char ':' ^+^ fmtType fmt
        fmtType (FPrim p) = PP.text $ case p of
          PUInt  -> "int"
          PSInt  -> "int"
          PChar -> "char"
          PLine _ -> "string"
          PWord _ -> "string"
        fmtType (FCustom typename) = str typename
        fmtType (FArray { arrElt = elt })
          | [(_,fmt)] <- mapMaybe getBinding elt =
            fmtType fmt ^+^ PP.text "array"
        fmtType (FMatrix { matElt = elt })
          | [(_,fmt)] <- mapMaybe getBinding elt =
            fmtType fmt ^+^ PP.text "array array"
        
          
