module Datum where

import Data.Map (Map)

type Fields = Map String Datum

data Datum = DInt    Int
           | DChar   Char
           | DString String
           | DArray  [Datum]
           | DMatrix [[Datum]] -- so inefficient wow
           | DRecord String Fields
           | DEnum   String String -- (type name, element)

