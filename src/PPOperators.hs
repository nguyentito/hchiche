module PPOperators where

import Data.Monoid
import qualified Data.Text.Lazy as T
import qualified Text.PrettyPrint.Leijen.Text as PP

-- Convention for operator names inspired by François Pottier's PPrint library
-- The problem with the default Wadler-Leijen operators is,
-- they look too much like functions on Applicatives
-- (the WL paper predates the McBride-Paterson one)

(^/^) = (PP.</>)
(^+^) = (PP.<+>)
(^$^) = (PP.<$>)
a ^$$^ b = a <> PP.line <> PP.line <> b
a ^/-^ b = PP.group $ a <> PP.nest 2 (PP.line <> b)

ppt = PP.text
ppc = PP.char
pps = ppt . T.pack

