module AST where

import Data.Monoid (Monoid, mempty, mappend)

-- customTypes is an association list and not a Map to keep track
-- of the order of type declarations which is important
-- we expect the size of the list to be small so lookups
-- will morally be O(1)


data HChiche = HChiche { customTypes :: CustomAssoc
                       , mainFunctionName :: String
                       , mainFunctionArgs :: [BindSep] }
            deriving (Eq, Ord, Show)

data CustomFormat' a = Record a
                     | Tuple a
                     | Enum [(Char, String)]
                     deriving (Eq, Ord, Show)

type CustomFormat = CustomFormat' [BindSep]
type CustomAssoc = [(String, CustomFormat)]


type Binding = (String, Format)

data BindSep = Bind Binding | Sep Separator
              deriving (Eq, Ord, Show)

getBinding :: BindSep -> Maybe Binding
getBinding (Bind b) = Just b
getBinding (Sep  _) = Nothing


data PrimFormat = PUInt | PSInt | PChar | PDigit
                | PLine IntExpr -- string on entire line
                | PWord IntExpr -- sequence of chars without spaces
                deriving (Eq, Ord, Show)

data Format = FPrim PrimFormat
            | FCustom String
            | FArray { arrLen :: IntExpr
                     , arrVar :: Maybe String
                     , arrElt :: [BindSep] }
            | FMatrix { matDim  :: (IntExpr, IntExpr)
                      , matVars :: Maybe (String, String)
                      , matElt  :: [BindSep] }
            deriving (Eq, Ord, Show)

data Separator = Newline | Space | NullSep
               deriving (Eq, Ord, Show)

instance Monoid Separator where
  mempty = NullSep
  Newline `mappend` _ = Newline
  _ `mappend` Newline = Newline
  NullSep `mappend` x = x
  x `mappend` NullSep = x
  Space `mappend` Space = Space


data IntExpr = IConst Int
             | IVar String
             | IOp Binop IntExpr IntExpr
             deriving (Eq, Ord, Show)

data Binop = Add | Sub | Mul
           deriving (Eq, Ord, Show, Enum, Bounded)

binops :: [Binop]
binops = [minBound .. maxBound]

binopName :: Binop -> String
binopName Add = "+"
binopName Sub = "-"
binopName Mul = "*"

