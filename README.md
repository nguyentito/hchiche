HChiche est un format de description d'entrées pour exercices
d'algorithmique effective (notamment ceux du concours
[Prologin](http://prologin.org/), et un générateur de codes C, C++,
Python, OCaml, etc. à partir des descriptions de haut niveau.

Il vise à complémenter
[Métalang](https://bitbucket.org/prologin/metalang) en étant plus
spécialisé (alors que Métalang est un langage de programmation
impératif généraliste).

